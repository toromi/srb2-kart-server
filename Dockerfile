FROM ubuntu:bionic

RUN apt-get update && \
  apt-get install -y \
  dirmngr \
  gnupg

# software-properties-common gives you `add-apt-repository`
# SUPER HEAVY!!!
RUN apt-get install -y software-properties-common

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BC359FFF5A04B56C41DBC134289CABAB043F53A7 && \
  add-apt-repository -y ppa:kartkrew/srb2kart && \
  apt-get update && \
  apt-get install -y srb2kart

# RUN apt-get install -y curl unzip && \
#   curl -L -o files.zip https://www.dropbox.com/sh/mknyku9x8vmn2l7/AACkMs-XcVdGOW0r077jnGf-a\?dl\=1 && \
#   unzip files.zip && \
#   mv WADs/* . && \
#   rm -r WADs && \
#   rm files.zip

COPY files/* /root/.srb2kart/

EXPOSE 5029

CMD ["/usr/games/srb2kart", "-dedicated"]
