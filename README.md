SRB2 Kart Server
================

Installation steps for Ubuntu.

```sh
# Install anything you may not have.
apt install -y git curl unzip docker-compose

# Clone the repo.
git clone https://gitlab.com/toromi/srb2-kart-server.git
cd srb2-kart-server

# Install the server files.
cd files
curl -L -o files.zip https://www.dropbox.com/sh/mknyku9x8vmn2l7/AACkMs-XcVdGOW0r077jnGf-a\?dl\=1
unzip files
mv WADs/* .
rm -r WADs files.zip

# Start the server.
docker-compose up -d

# Open your firewall.
ufw allow 5029
ufw allow 5029/tcp # probably only 1 of TCP or UDP is needed.
ufw allow ssh # Just in case...
ufw enable
ufw status
```
